TwoLevels64-bitRoaringBitmap
=============

This bitmap's scheme is conceived to efficiently store 64-bit integers. Similarly to Roaring, this model adopts one level of indexing above the containers. Each entry of the first level stores a 64-bit key and a pointer to a Container. The adopted container's model is the same as the Roaring's one. The high 48 bits of a key store the common high 48 bits of an integers' group sharing these high bits, and the remaining 16 bits of a key save the cardinality of that group. Finally, the least 16 bits of the integers are stored in the container associated to the first level entry.    

License
------
This code is licensed under Apache License, Version 2.0 (ASL2.0). 

Usage
------

* Get java
* Get maven 2

* mvn compile will compile
* mvn test will run the unit tests
* mvn package will package in a jar (found in target)

Funding 
----------

This work was supported by NSERC grant number 26143.
