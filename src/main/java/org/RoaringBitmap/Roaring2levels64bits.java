/*
 * (c) Samy Chambi, Daniel Lemire.
 */

package org.RoaringBitmap;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @author Samy Chambi
 *
 */
/**
 * RoaringBitmap for 64-bit integers, a compressed alternative to the BitSet.
 * 
 * <pre>
 * {@code
 *      import org.hierarchicalroaringbitmap.*;
 *       
 *      //...
 *      
 *      LazyHierarRoaringBitmap rr = LazyHierarRoaringBitmap.bitmapOf(1,2,3,1000);
 *      LazyHierarRoaringBitmap rr2 = new LazyHierarRoaringBitmap();
 *      for(int k = 4000; k<4255;++k) rr2.add(k);
 *      LazyHierarRoaringBitmap rror = LazyHierarRoaringBitmap.or(rr, rr2);
 * }
 * </pre>
 *
 *
 * 
 */
public class Roaring2levels64bits implements Cloneable, Serializable, Iterable<Long>, Externalizable, 
												ImmutableBitmapDataProvider {

    //private static final long serialVersionUID = 6L;
    public Element[] highLowContainer;
    private static final int INITIAL_CAPACITY = 4;
    int size;
    
    public int size(){
    	return size;
    }
    
    

    /**
     * Bitwise OR (union) operation. The provided bitmaps are *not*
     * modified. This operation is thread-safe as long as the provided
     * bitmaps remain unchanged.
     * 
     * If you have more than 2 bitmaps, consider using the
     * FastAggregation class.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation     
     */
    	public static Roaring2levels64bits or(final Roaring2levels64bits x1,
                                   final Roaring2levels64bits x2) {
    	if(x1.size==0)
    		return x2.clone();
    	if(x2.size==0)
    		return x1.clone();
    	final int length1 = x1.size, length2 = x2.size;
    	final int length=length1+length2;
    	final Roaring2levels64bits answer = new Roaring2levels64bits(length);
        int pos1=0, pos2=0, pos=0;
        long s1 = x1.highLowContainer[pos1].getKey();
        long s2 = x2.highLowContainer[pos2].getKey();        
        do {
                if (s1 < s2) {
                    answer.highLowContainer[pos]=x1.highLowContainer[pos1].clone();
                    ++pos1;
                    ++pos;
                    if (pos1 == length1)
                        break;
                    s1 = x1.highLowContainer[pos1].getKey();
                } else if (s1 > s2) {
                	answer.highLowContainer[pos]=x2.highLowContainer[pos2].clone();
                    ++pos2;
                    ++pos;
                    if (pos2 == length2)
                        break;
                    s2 = x2.highLowContainer[pos2].getKey();
                } else {
                	answer.highLowContainer[pos]= new Element(s1, x1.highLowContainer[pos1].getContainer().or(
                            x2.highLowContainer[pos2].getContainer()));                    
                    ++pos1;
                    ++pos2;
                    ++pos;
                    if (pos1 == length1 || pos2 == length2)
                        break;
                    s1 = x1.highLowContainer[pos1].getKey();
                    s2 = x2.highLowContainer[pos2].getKey();
                }
            } while(true);
        
        while(pos1 < length1) {
			answer.highLowContainer[pos]=x1.highLowContainer[pos1].clone();
			++pos;
			++pos1;
		}        
        while(pos2<length2) {
        	answer.highLowContainer[pos]=x2.highLowContainer[pos2].clone();
			++pos;
        	++pos2;
        }			
        answer.size=pos;
        if(pos<length)
        	answer.highLowContainer = Arrays.copyOf(answer.highLowContainer, pos);
        
        return answer;
    }

    /**
     * Bitwise ANDNOT (difference) operation. The provided bitmaps are *not*
     * modified. This operation is thread-safe as long as the provided
     * bitmaps remain unchanged.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation
     */
   /* public static LazyHierarRoaringBitmap andNot(final LazyHierarRoaringBitmap x1,
                                       final LazyHierarRoaringBitmap x2) {
        final LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        int pos1 = 0, pos2 = 0;
        final int length1 = x1.highLowContainer.size(), length2 = x2.highLowContainer
                .size();
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = x1.highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);
            do {
                if (s1 < s2) {
                    answer.highLowContainer.appendCopy(
                            x1.highLowContainer, pos1);
                    pos1++;
                    if (pos1 == length1)
                        break main;
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = x1.highLowContainer
                            .getContainerAtIndex(pos1)
                            .andNot(x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0)
                        answer.highLowContainer.append(s1, c);
                    pos1++;
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break main;
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            } while (true);
        }
        if (pos2 == length2) {
            answer.highLowContainer.appendCopy(x1.highLowContainer, pos1, length1);
        }
        return answer;
    } */

    /**
     * Generate a bitmap with the specified values set to true. The provided
     * integers values don't have to be in sorted order, but it may be
     * preferable to sort them from a performance point of view.
     *
     * @param dat set values
     * @return a new bitmap
     */
    public static Roaring2levels64bits bitmapOf(final long... dat) {
        final Roaring2levels64bits ans = new Roaring2levels64bits();
        for (final long i : dat)
            ans.add(i);
        return ans;
    }

    /**
     * Complements the bits in the given range, from rangeStart (inclusive)
     * rangeEnd (exclusive). The given bitmap is unchanged.
     *
     * @param bm         bitmap being negated
     * @param rangeStart inclusive beginning of range
     * @param rangeEnd   exclusive ending of range
     * @return a new Bitmap
     */
  /*  public static LazyHierarRoaringBitmap flip(LazyHierarRoaringBitmap bm,final int rangeStart, final int rangeEnd) {
        if (rangeStart >= rangeEnd) {
            return bm.clone();
        }

        LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        final short hbStart = Util.highbits(rangeStart);
        final short lbStart = Util.lowbits(rangeStart);
        final short hbLast = Util.highbits(rangeEnd - 1);
        final short lbLast = Util.lowbits(rangeEnd - 1);

        // copy the containers before the active area
        answer.highLowContainer.appendCopiesUntil(bm.highLowContainer, hbStart);

        int max = Util.toIntUnsigned(Util.maxLowBit());
        for (short hb = hbStart; hb <= hbLast; ++hb) {
            final int containerStart = (hb == hbStart) ? Util.toIntUnsigned(lbStart) : 0;
            final int containerLast = (hb == hbLast) ? Util.toIntUnsigned(lbLast) : max;

            final int i = bm.highLowContainer.getIndex(hb);
            final int j = answer.highLowContainer.getIndex(hb);
            assert j < 0;

            if (i >= 0) {
                Container c = bm.highLowContainer.getContainerAtIndex(i).not(containerStart, containerLast);
                if (c.getCardinality() > 0)
                    answer.highLowContainer.insertNewKeyValueAt(-j - 1, hb, c);

            } else { // *think* the range of ones must never be
                // empty.
                answer.highLowContainer.insertNewKeyValueAt(-j - 1, hb, Container.rangeOfOnes(
                                containerStart, containerLast)
                );
            }
        }
        // copy the containers after the active area.
        answer.highLowContainer.appendCopiesAfter(bm.highLowContainer, hbLast);

        return answer;
    } */

    /**
     * Bitwise OR (union) operation. The provided bitmaps are *not*
     * modified. This operation is thread-safe as long as the provided
     * bitmaps remain unchanged.
     * 
     * If you have more than 2 bitmaps, consider using the
     * FastAggregation class.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation
     */
    public static Roaring2levels64bits and(final Roaring2levels64bits x1,
                                   final Roaring2levels64bits x2) {
    	if(x1.size==0 || x2.size==0)
    		return new Roaring2levels64bits();
    	//System.out.println("and ::");
    	final int length1 = x1.size, length2 = x2.size;
    	int length = length1<length2 ? length1:length2;
    	final Roaring2levels64bits answer = new Roaring2levels64bits(length);
        int pos1=0, pos2=0, pos=0;
        long s1 = x1.highLowContainer[pos1].getKey();
        long s2 = x2.highLowContainer[pos2].getKey();
        //System.out.println("s1= "+s1+", s2 = "+s2);
        do {
                if (s1 < s2) {
                	//System.out.println("s1<s2 ::");
                    pos1=advanceUntil(x1.highLowContainer, s2, pos1, length1);
                    if (pos1 == length1)
                        break;
                    s1 = x1.highLowContainer[pos1].getKey();
                    //System.out.println("taken s1 :: "+s1);
                } else if (s1 > s2) {
                	//System.out.println("s2<s1 ::");
                    pos2=advanceUntil(x2.highLowContainer, s1, pos2, length2);
                    if (pos2 == length2)
                        break;
                    s2 = x2.highLowContainer[pos2].getKey();
                    //System.out.println("taken s2 :: "+s2);
                } else {
                	//System.out.println("equivalence");
                    answer.highLowContainer[pos] = new Element(s1, x1.highLowContainer[pos1].getContainer()
									.and(x2.highLowContainer[pos2].getContainer()));
                    ++pos1;
                    ++pos2;
                    ++pos;
                    if (pos1 == length1 || pos2 == length2)
                        break;
                    s1 = x1.highLowContainer[pos1].getKey();
                    s2 = x2.highLowContainer[pos2].getKey();
                    //System.out.println("eq :: s1="+s1+", s2="+s2);
                }
           } while(true);
        answer.size = pos;
        if(pos<length)
        	answer.highLowContainer = Arrays.copyOf(answer.highLowContainer, pos);
        
        return answer;
    }
    
    /**
     * Find the smallest integer index larger than pos such that array[index].key sup. or equal x.
     * If none can be found, return size. Based on code by O. Kaser.
     * 
     * @param array array of elements. 
     * @param x minimal value.
     * @param pos index to exceed.
     * @param size the number of elements in the manipulated array.
     * @return the smallest index greater than pos such that array[index].key is at least as large
     * as min, or size if it is not possible.
     */
    protected static int advanceUntil(Element[] array, long x, int pos, int size) {
        int lower = pos + 1;

        // special handling for a possibly common sequential case
        if (lower >= size || array[lower].key>>>16 >= x) {
            return lower;
        }

        int spansize = 1; // could set larger
        // bootstrap an upper limit

        while (lower + spansize < size && array[lower + spansize].key>>>16 < x)
            spansize *= 2; // hoping for compiler will reduce to shift
        int upper = (lower + spansize < size) ? lower + spansize : size - 1;

        // maybe we are lucky (could be common case when the seek ahead
        // expected to be small and sequential will otherwise make us look bad)
        if (array[upper].key>>>16 == x) {
            return upper;
        }

        if (array[upper].key>>>16 < x) {// means array has no item key >= x
            return size;
        }

        // we know that the next-smallest span was too small
        lower += (spansize / 2);

        // else begin binary search
        // invariant: array[lower]<x && array[upper]>x
        while (lower + 1 != upper) {
            int mid = (lower + upper) / 2;
            if (array[mid].key>>>16 == x)
                return mid;
            else if (array[mid].key>>>16 < x)
                lower = mid;
            else
                upper = mid;
        }
        return upper;
    }
    
    /**
     * Rank returns the number of integers that are smaller or equal to x (Rank(infinity) would be GetCardinality()).
     * @param x upper limit
     *
     * @return the rank
     */
   
    /* public int rank(int x) {
        int size = 0;
        int xhigh = Util.highbits(x);
        
        for (int i = 0; i < this.highLowContainer.size(); i++) {
            short key =  this.highLowContainer.getKeyAtIndex(i);
            if( key < xhigh )      
              size += this.highLowContainer.getContainerAtIndex(i).getCardinality();
            else 
                return size + this.highLowContainer.getContainerAtIndex(i).rank(Util.lowbits(x));
        }
        return size;
    }*/
    

    /**
     * Return the jth value stored in this bitmap.
     * 
     * @param j index of the value 
     *
     * @return the value
     */
    /*public int select(int j)
        int leftover = j;
        for (int i = 0; i < this.highLowContainer.length; i++) {
            Container c = this.highLowContainer.getContainerAtIndex(i);
            int thiscard = c.getCardinality();
            if(thiscard > leftover) {
                int keycontrib = this.highLowContainer.getKeyAtIndex(i)<<16;
                int lowcontrib = Util.toIntUnsigned(c.select(leftover));
                return  lowcontrib + keycontrib;
            }
            leftover -= thiscard;
        }
        throw new IllegalArgumentException("select "+j+" when the cardinality is "+this.getCardinality());
    }
*/
    
    /**
     * Bitwise XOR (symmetric difference) operation. The provided bitmaps
     * are *not* modified. This operation is thread-safe as long as the
     * provided bitmaps remain unchanged.
     * 
     * If you have more than 2 bitmaps, consider using the
     * FastAggregation class.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation
     * @see FastAggregation#xor(LazyHierarRoaringBitmap...)
     * @see FastAggregation#horizontal_xor(LazyHierarRoaringBitmap...)
     */
  /*  public static LazyHierarRoaringBitmap xor(final LazyHierarRoaringBitmap x1, final LazyHierarRoaringBitmap x2) {
        final LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        int pos1 = 0, pos2 = 0;
        final int length1 = x1.highLowContainer.size(), length2 = x2.highLowContainer.size();

        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = x1.highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);

            while (true) {
                if (s1 < s2) {
                    answer.highLowContainer.appendCopy(x1.highLowContainer, pos1);
                    pos1++;
                    if (pos1 == length1) {
                        break main;
                    }
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    answer.highLowContainer.appendCopy(x2.highLowContainer, pos2);
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = x1.highLowContainer.getContainerAtIndex(pos1).xor(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0)
                        answer.highLowContainer.append(s1, c);
                    pos1++;
                    pos2++;
                    if ((pos1 == length1)
                            || (pos2 == length2)) {
                        break main;
                    }
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            }
        }
        if (pos1 == length1) {
            answer.highLowContainer.appendCopy(x2.highLowContainer, pos2, length2);
        } else if (pos2 == length2) {
            answer.highLowContainer.appendCopy(x1.highLowContainer, pos1, length1);
        }

        return answer;
    }*/    

    /**
     * Create an empty bitmap
     */
    public Roaring2levels64bits() {
    	highLowContainer = new Element[INITIAL_CAPACITY];
    }
    
    public Roaring2levels64bits(int size){
    	highLowContainer = new Element[size];
    }        

    /**
     * set the value to "true", whether it already appears or not.
     *
     * @param x integer value
     * @return true if the inserted long wasn't already in the bitmap. False otherwise.
     */
    public boolean add(final long x) {
    	long hb = x>>>16;
    	short lb = (short) x;
    	int i=getIndex(hb);
    	if(i>=0) {
    		Container c = getContainer(i);
    		int oldCard = c.getCardinality();
    		c=c.add(lb);    		
    		if(c.getCardinality()>oldCard) {
    			setContainer(i, c);
    			highLowContainer[i].setCardinality();
    			return true;
    		}
    	}
    	else {
    			extend();
    			ArrayContainer ar = new ArrayContainer();
    			ar.add(lb);
    			insertNewKeyValue(hb, ar, -i-1);
    			++size;
    			return true;
    	}
    	return false;
    }       
    
    public void insertNewKeyValue(long key, Container value, int pos) {
    	System.arraycopy(highLowContainer, pos, highLowContainer, pos+1, size-pos);
    	highLowContainer[pos]=new Element(key, value);
    }
    
    public int getIndex(long hb){
    	int pos = UtilRoaring2lvls.BinarySearchLongs(highLowContainer, 0, size, hb);
    	return pos;
    }
    
    public Container getContainer(int i){
    	return highLowContainer[i].getContainer();
    }
    
    public void setContainer(int i, Container c){
    	highLowContainer[i].setContainer(c);
    }
    
    /**
     * Extend this first level array.
     */
    private void extend() {
    	if(this.size>=this.highLowContainer.length){    		
    		int newCapacity;
            if (this.highLowContainer.length < 1024)
                newCapacity = 2 * (this.size+1);
            else 
                newCapacity = 5 * (this.size + 1) / 4;
            this.highLowContainer = Arrays.copyOf(this.highLowContainer, newCapacity);
    	}    	
    }

    /**
     * In-place bitwise AND (intersection) operation. The current bitmap is
     * modified.
     *
     * @param x2 other bitmap
     */
   /* public void and(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();
                
                 // TODO: This could be optimized quite a bit when one bitmap is
                 // much smaller than the other one.
                 
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);
            do {
                if (s1 < s2) {
                    highLowContainer.removeAtIndex(pos1);
                    --length1;
                    if (pos1 == length1)
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2)
                        break main;
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = highLowContainer.getContainerAtIndex(pos1).iand(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0) {
                        this.highLowContainer.setContainerAtIndex(pos1, c);
                        pos1++;
                    } else {
                        highLowContainer.removeAtIndex(pos1);
                        --length1;
                    }
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            } while (true);
        }
        highLowContainer.resize(pos1);
    } */
    
    /**
     * In-place bitwise ANDNOT (difference) operation. The current bitmap is
     * modified.
     *
     * @param x2 other bitmap
     */
  /*  public void andNot(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);
            do {
                if (s1 < s2) {
                    pos1++;
                    if (pos1 == length1)
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = highLowContainer.getContainerAtIndex(pos1).iandNot(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0) {
                        this.highLowContainer.setContainerAtIndex(pos1, c);
                        pos1++;
                    } else {
                        highLowContainer.removeAtIndex(pos1);
                        --length1;
                    }
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            } while (true);
        }
    } */

    /**
     * reset to an empty bitmap; result occupies as much space a newly
     * created bitmap.
     */
   /* public void clear() {
        highLowContainer = new RoaringArray(); // lose references
    } */

    @Override
    public Roaring2levels64bits clone() {
        try {
            final Roaring2levels64bits x = (Roaring2levels64bits) super.clone();
            return x;
        } catch (final CloneNotSupportedException e) {
            throw new RuntimeException("shouldn't happen with clone", e);
        }
    }

    /**
     * Checks whether the value in included, which is equivalent to checking
     * if the corresponding bit is set (get in BitSet class).
     *
     * @param x integer value
     * @return whether the integer value is included.
     */
    @Override
    public boolean contains(final long x) {    	
    	final int pos = UtilRoaring2lvls.BinarySearchLongs(highLowContainer, 0, size, x>>>16);    	
    	return pos<0 ? false : highLowContainer[pos].getContainer().contains((short)x);
    }

    /*
     * Deserialize (retrieve) this bitmap.
     * 
     * The current bitmap is overwritten.
     *
     * @param in the DataInput stream
     * @throws IOException Signals that an I/O exception has occurred.
     *
    public void deserialize(DataInput in) throws IOException {
        this.highLowContainer.deserialize(in);
    } */

    @Override
    public boolean equals(Object o) {
        if (o instanceof Roaring2levels64bits) {
            final Roaring2levels64bits srb = (Roaring2levels64bits) o;
            return Arrays.equals(srb.highLowContainer, this.highLowContainer);
        }
        return false;
    }


    /**
     * Modifies the current bitmap by complementing the bits in the given
     * range, from rangeStart (inclusive) rangeEnd (exclusive).
     *
     * @param rangeStart inclusive beginning of range
     * @param rangeEnd   exclusive ending of range
     */
    /*public void flip(final int rangeStart, final int rangeEnd) {
        if (rangeStart >= rangeEnd)
            return; // empty range

        final short hbStart = Util.highbits(rangeStart);
        final short lbStart = Util.lowbits(rangeStart);
        final short hbLast = Util.highbits(rangeEnd - 1);
        final short lbLast = Util.lowbits(rangeEnd - 1);

        final int max = Util.toIntUnsigned(Util.maxLowBit());
        for (short hb = hbStart; hb <= hbLast; ++hb) {
            // first container may contain partial range
            final int containerStart = (hb == hbStart) ? Util.toIntUnsigned(lbStart) : 0;
            // last container may contain partial range
            final int containerLast = (hb == hbLast) ? Util.toIntUnsigned(lbLast) : max;
            final int i = highLowContainer.getIndex(hb);

            if (i >= 0) {
                final Container c = highLowContainer.getContainerAtIndex(i).inot(
                                containerStart, containerLast);
                if (c.getCardinality() > 0)
                    highLowContainer.setContainerAtIndex(i, c);
                else
                    highLowContainer.removeAtIndex(i);
            } else {
                highLowContainer.insertNewKeyValueAt(-i - 1,hb, Container.rangeOfOnes(
                        containerStart, containerLast)
                );
            }
        }
    }*/

    /**
     * Returns the number of distinct integers added to the bitmap (e.g.,
     * number of bits set).
     *
     * @return the cardinality
     */
    public int getCardinality() {
        int size = 0;
        for (int i = 0; i < this.size; i++) 
            size += this.highLowContainer[i].getContainer().getCardinality();
        return size;
    }

    /**
     * @return a custom iterator over set bits, the bits are traversed
     * in ascending sorted order
     */
   /* public IntIterator getIntIterator() {
        return new RoaringIntIterator();
    }*/

    /**
     * @return a custom iterator over set bits, the bits are traversed
     * in descending sorted order
     */
   /* public IntIterator getReverseIntIterator() {
        return new RoaringReverseIntIterator();
    }*/

    /**
     * Estimate of the memory usage of this data structure. This
     * can be expected to be within 1% of the true memory usage.
     *
     * @return estimated memory usage.
     */
   /* public int getSizeInBytes() {
        int size = 8;
        for (int i = 0; i < this.highLowContainer.size(); i++) {
            final Container c = this.highLowContainer.getContainerAtIndex(i);
            size += 2 + c.getSizeInBytes();
        }
        return size;
    }*/

    @Override
    public int hashCode() {
        	int hashvalue = 0;
        	for(int k = 0; k < this.highLowContainer.length; ++k)
        		hashvalue = 31 * hashvalue + this.highLowContainer[k].hashCode();
        	return hashvalue;
    }

    /**
     * iterate over the positions of the true values.
     *
     * @return the iterator
     */
    @Override
    public Iterator<Long> iterator() {
        return new Iterator<Long>() {
        	private ShortIterator iter;

            private int pos = 0;
            
            private Iterator<Long> init() {
                nextContainer();
                return this;
            }

            @Override
            public boolean hasNext() {
            	if(iter.hasNext())
            		return true;
                return pos < Roaring2levels64bits.this.size();
            }

            private void nextContainer() {
                if (pos < Roaring2levels64bits.this.size()) {
                    iter = Roaring2levels64bits.this.highLowContainer[pos].getContainer().getShortIterator();                
                }
            }

            @Override
            public Long next() {
                long x = UtilRoaring2lvls.toIntUnsigned(iter.next()) | 
                		Roaring2levels64bits.this.highLowContainer[pos].getKey() << 16;
                if (!iter.hasNext()) {
                    ++pos;
                    nextContainer();
                }
                return x;
            }

            @Override
            public LongIterator clone() {
                try {
                    RoaringLongIterator x = (RoaringLongIterator) super.clone();
                    x.iter =  this.iter.clone();
                    return x;
                } catch (CloneNotSupportedException e) {
                    return null;// will not happen
                }
            }
            
        }.init();
    }

    /*
     * Checks whether the bitmap is empty.
     * 
     * @return true if this bitmap contains no set bit
     *
    public boolean isEmpty() {
    	return highLowContainer.size() == 0;
    }*/


    /*
     * In-place bitwise OR (union) operation. The current bitmap is
     * modified.
     *
     * @param x2 other bitmap
     *
    public void or(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);

            while (true) {
                if (s1 < s2) {
                    pos1++;
                    if (pos1 == length1) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    highLowContainer.insertNewKeyValueAt(pos1, s2, x2.highLowContainer.getContainerAtIndex(pos2)
                    );
                    pos1++;
                    length1++;
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    this.highLowContainer.setContainerAtIndex(pos1, highLowContainer.getContainerAtIndex(
                                    pos1).ior(x2.highLowContainer.getContainerAtIndex(pos2))
                    );
                    pos1++;
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2)) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            }
        }
        if (pos1 == length1) {
            highLowContainer.appendCopy(x2.highLowContainer, pos2, length2);
        }
    }*/

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        //this.highLowContainer.readExternal(in);
    }

    /**
     * If present remove the specified integers (effectively, sets its bit
     * value to false)
     *
     * @param x integer value representing a position in the bitmap
     */
    public boolean remove(final long x) {
        final long hb = x >>> 16;
        final int i = getIndex(hb);
        if (i < 0)
            return false;
        Container c = getContainer(i);
        int oldCard = c.getCardinality();
        setContainer(i, c.remove((short) x));
        if (c.getCardinality() == 0) {
            removeAtIndex(i);
            return true;
        }
        if (c.getCardinality() < oldCard)
        	return true;
        return false;
    }
    
    /**
     * Removes the i th entry in the array by shifting entries in ranges i+1 to size by
     * one position at left. The size field is decremented by one after that.
     * highLowContainer array is supposed to have at least one element.  
     * @param i the index of the entry to remove from the array.
     */
    public void removeAtIndex(int i) {
    	System.arraycopy(highLowContainer, i+1, highLowContainer, i, size-i-1);
    	size--;
    }
        
    public long getKeyAtIndex(int i){
    	return highLowContainer[i].getKey();
    }

    /**
     * Serialize this bitmap.
     * 
     * The current bitmap is not modified.
     *
     * @param out the DataOutput stream
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void serialize(DataOutput out) throws IOException {
        //this.highLowContainer.serialize(out);
    }

    /**
     * Report the number of bytes required to serialize this bitmap.
     * This is the number of bytes written out when using the serialize
     * method. When using the writeExternal method, the count will be
     * higher due to the overhead of Java serialization.
     *
     * @return the size in bytes
     */
   /* public int serializedSizeInBytes() {
        return this.highLowContainer.serializedSizeInBytes();
    }*/
    
    /**
     * Create a new Roaring bitmap containing at most maxcardinality integers.
     * 
     * @param maxcardinality maximal cardinality
     * @return a new bitmap with cardinality no more than maxcardinality
     */
   /* public LazyHierarRoaringBitmap limit(int maxcardinality) {
        LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        int currentcardinality = 0;        
        for (int i = 0; (currentcardinality < maxcardinality) && ( i < this.highLowContainer.size()); i++) {
            Container c = this.highLowContainer.getContainerAtIndex(i);
            if(c.getCardinality() + currentcardinality <= maxcardinality) {
               answer.highLowContainer.appendCopy(this.highLowContainer, i);
               currentcardinality += c.getCardinality();
            }  else {
                int leftover = maxcardinality - currentcardinality;
                Container limited = c.limit(leftover);
                answer.highLowContainer.append(this.highLowContainer.getKeyAtIndex(i),limited );
                break;
            }
        }
        return answer;
    }*/

    /**
     * Return a sorted array of long integers values.
     *
     * @return Return a sorted array of the long integers stored in this bitmap.
     */
    public Object[] toArray() {
    	ArrayList<Long> longs = new ArrayList<Long>();    	
    	for(int i=0; i<size; i++) {
    		long hb = highLowContainer[i].getKey();
    		Container c = highLowContainer[i].getContainer();
         	ShortIterator sit = c.getShortIterator();
         	while(sit.hasNext())
         		longs.add(hb<<16|UtilRoaring2lvls.toIntUnsigned(sit.next()));
         }
    	Object[] a = longs.toArray();
    	Arrays.sort(a);
        return a;
    }

    /**
     * A string describing the bitmap.
     *
     * @return the string
     */
    /*@Override
    public String toString() {
    	 final StringBuffer answer = new StringBuffer();
         final StringBuffer firstLevel = new StringBuffer();
         final StringBuffer secLevel = new StringBuffer();
         final StringBuffer containers = new StringBuffer();
         
         firstLevel.append("{");
         for(int i=0; i<highLowContainer.length; i++) {
         	firstLevel.append(Util.intToLongUnsigned(highLowContainer[i].getValue())+", ");
         	secLevel.append("{");
         	for(int j=0; j<highLowContainer[i].getChilds().length; j++) {
         		secLevel.append(Util.shortToLongUnsigned(highLowContainer[i].getChilds()[j].getValue())+", ");
         		containers.append("{");
         		ShortIterator sit = highLowContainer[i].getChilds()[j].getChilds().getShortIterator();
         		while(sit.hasNext())
         			containers.append(Util.toIntUnsigned(sit.next())+", ");
         		containers.append("} ");
         	}
         	secLevel.append("} ");
         }
         firstLevel.append("} ");
         
         answer.append(firstLevel.toString()+"\n"+secLevel.toString()+"\n"+containers.toString());
        
        return answer.toString();
    }*/

	@Override
	public LongIterator getIntIterator() {
		// TODO Auto-generated method stub
		return new RoaringLongIterator();
	}

	@Override
	public int serializedSizeInBytes() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public LongIterator getReverseIntIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSizeInBytes() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}	

	@Override
	public int rank(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ImmutableBitmapDataProvider limit(int x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int select(int j) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static class Element implements Cloneable, Comparable<Element> {
        private long key;
        private Container value = null;

        public Element(long key, Container value) {
            this.key = key<<16;
            this.value = value;
            setCardinality();
        }
        
        public long getKey() {
        	return key>>>16;
        }
        
        public int getCardinality() {
        	return UtilRoaring2lvls.toIntUnsigned((short)key)+1;
        }
        
        public Container getContainer() {
        	return value;
        }
        
        public void setCardinality(short i) {
        	key &= 0xFFFFFFFFFFFF0000L;
        	key |= i;
        }
        
        public void setCardinality() {
        	key &= 0xFFFFFFFFFFFF0000L;
        	key |= value.getCardinality()-1;
        }
        
        public void setContainer(Container c){
        	value = c;
        }

        @Override
        public int hashCode() {
        	return (int) (key * 0xF0F0F0 + value.hashCode());
        }
        
        @Override
        public boolean equals(Object o) {
        	if(o instanceof Element) {
        		Element e = (Element) o;
        		return (e.key == key) && e.value.equals(value);
        	}
        	return false;
        }
        
        @Override
        public Element clone() {
            Element c=null;
			try {
				c = (Element) super.clone();
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
            //c.key copied by super.clone
            c.value = this.value.clone();
            return c;
        }
        
        @Override
		public int compareTo(Element o) {
			return (int) (this.key - o.key);
		}
    }

    /**
     * Recover allocated but unused memory.
     */
   /* public void trim() {
        for (int i = 0; i < this.highLowContainer.size(); i++) {
            this.highLowContainer.getContainerAtIndex(i).trim();
        }
    } */
    
    
	/**
     * In-place bitwise XOR (symmetric difference) operation. The current
     * bitmap is modified.
     *
     * @param x2 other bitmap
     */
    /*public void xor(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();

        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);

            while (true) {
                if (s1 < s2) {
                    pos1++;
                    if (pos1 == length1) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    highLowContainer.insertNewKeyValueAt(pos1, s2, x2.highLowContainer.getContainerAtIndex(pos2));
                    pos1++;
                    length1++;
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = highLowContainer.getContainerAtIndex(pos1).ixor(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0) {
                        this.highLowContainer.setContainerAtIndex(pos1, c);
                        pos1++;
                    } else {
                        highLowContainer.removeAtIndex(pos1);
                        --length1;
                    }
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2)) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            }
        }
        if (pos1 == length1) {
            highLowContainer.appendCopy(x2.highLowContainer, pos2, length2);
        }
    }*/


    private final class RoaringLongIterator implements LongIterator {
        private ShortIterator iter;

        private int pos = 0;
        
        private RoaringLongIterator() {
            nextContainer();
        }

        @Override
        public boolean hasNext() {
        	if(iter.hasNext())
        		return true;
            return pos < Roaring2levels64bits.this.size();
        }

        private void nextContainer() {
            if (pos < Roaring2levels64bits.this.size()) {
                iter = Roaring2levels64bits.this.highLowContainer[pos].getContainer().getShortIterator();                
            }
        }

        @Override
        public long next() {
            long x = UtilRoaring2lvls.toIntUnsigned(iter.next()) | 
            		Roaring2levels64bits.this.highLowContainer[pos].getKey() << 16;
            if (!iter.hasNext()) {
                ++pos;
                nextContainer();
            }
            return x;
        }

        @Override
        public LongIterator clone() {
            try {
                RoaringLongIterator x = (RoaringLongIterator) super.clone();
                x.iter =  this.iter.clone();
                return x;
            } catch (CloneNotSupportedException e) {
                return null;// will not happen
            }
        }
    } 
    
   /* private final class RoaringReverseIntIterator implements IntIterator {

        int hs = 0;
        ShortIterator iter;
        // don't need an int because we go to 0, not Short.MAX_VALUE, and signed shorts underflow well below zero
        short pos = (short) (LazyHierarRoaringBitmap.this.highLowContainer.size() - 1);

        private RoaringReverseIntIterator() {
            nextContainer();
        }

        @Override
        public boolean hasNext() {
            return pos >= 0;
        }

        private void nextContainer() {
            if (pos >= 0) {
                iter = LazyHierarRoaringBitmap.this.highLowContainer.getContainerAtIndex(pos).getReverseShortIterator();
                hs = Util.toIntUnsigned(LazyHierarRoaringBitmap.this.highLowContainer.getKeyAtIndex(pos)) << 16;
            }
        }

        @Override
        public int next() {
            final int x = Util.toIntUnsigned(iter.next()) | hs;
            if (!iter.hasNext()) {
                --pos;
                nextContainer();
            }
            return x;
        }

        @Override
        public IntIterator clone() {
            try {
                RoaringReverseIntIterator clone = (RoaringReverseIntIterator) super.clone();
                clone.iter =  this.iter.clone();
                return clone;
            } catch (CloneNotSupportedException e) {
                return null;// will not happen
            }
        }

    } */
}





